package net.gejza.bamboo.plugins.gnutools.tasks;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Created by gejza on 4.5.14.
 */
public class GnuMakeTaskConfigurator extends AbstractGnuToolsConfigurator {
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TARGET = "target";

    static {
        FIELDS_TO_COPY.add(TARGET);
    }

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (StringUtils.isEmpty(params.getString(AbstractGnuToolsConfigurator.RUNTIME))) {
            errorCollection.addError(AbstractGnuToolsConfigurator.RUNTIME, super.textProvider.getText("gnutools.make.runtime.error"));
        }
        //if (StringUtils.isEmpty(params.getString(AbstractGnuToolsConfigurator.COMMAND))) {
        //    errorCollection.addError(AbstractGnuToolsConfigurator.COMMAND, super.textProvider.getText("gnumake.command.error"));
        //}
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
