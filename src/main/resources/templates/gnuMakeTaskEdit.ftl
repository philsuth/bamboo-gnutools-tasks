[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='make' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='runtime'
list=uiConfigBean.getExecutableLabels('make')
extraUtility=addExecutableLink required=true /]

[@s.textarea labelKey='gnutools.make.target' name='target' rows='2' cssClass="resizable-long-field" /]
[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
[@ww.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]