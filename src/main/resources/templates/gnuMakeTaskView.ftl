[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[@ww.label labelKey='executable.type' name='runtime' /]
[@ww.label labelKey='gnutools.make.target' name='target' /]
[@ww.label labelKey='builder.common.env' name='environmentVariables' hideOnNull='true'/]
[@ww.label labelKey='builder.common.sub' name='workingSubDirectory' hideOnNull='true' /]

